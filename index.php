<?php
//Create a php file that prints the current date and time
//La siguiente funcion recibe como parametro la zona horaria como un string
date_default_timezone_set('America/Costa_Rica');

?>
<h1>Fecha y Hora actual</h1>

<?=date('m/d/y g:ia');?>
<br>
<br>
<a href="./static.html">Ir a página estática</a>
</body>
</html>